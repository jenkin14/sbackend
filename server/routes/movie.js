const express=require('express');
const utils=require('../utils');
const db=require('../db');
const router=express.Router();
router.post('/addmovie',(req,res)=>{ 
    const{movie_id,movie_title,movie_release_date,movie_time,director_name}=req.body;
    console.log(movie_id);
    const statement=`insert into movie(movie_id, movie_title,movie_release_date,movie_time,director_name) values(?,?,?,?,?);`;
    db.Pool.query(statement,[movie_id,movie_title,movie_release_date,movie_time,director_name],(err,data)=>{
        res.send(utils.createResult(err,data));
    })
})
router.get('/getallmovie', (req,res)=>{
    const statement=`select * from movie`;
    db.Pool.query(statement,(err,data)=>{
        res.send(utils.createResult(err,data));
    })
})
router.put('/updatemovie/:movie_id',(req,res)=>{ 
    const {movie_id}=req.params;
    const{movie_release_date,movie_time}=req.body;
    const statement=`update movie set movie_release_date=?, movie_time=? where movie_id=?;`;
    db.Pool.query(statement,[movie_release_date,movie_time,movie_id],(err,data)=>{
        res.send(utils.createResult(err,data));
    })
})
module.exports=router;

router.delete('/deletemovie/:movie_id',(req,res)=>{ 
    const {movie_id}=req.params;
    const statement=`delete from movie where movie_id=?;`;
    db.Pool.query(statement,[movie_id],(err,data)=>{
        res.send(utils.createResult(err,data));
    })
})