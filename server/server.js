const express =require('express');
const cors =require('cors')
const app=express();
app.use(cors());
app.use(express.json());

const movieRouter = require('./routes/movie')

app.use('/movie', movieRouter)
app.listen(4000,'0.0.0.0',()=>{
    console.log('server is started on port 4000');
})